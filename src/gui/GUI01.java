package gui;

import cricket.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI01 {
    public static void main(String[] args) {
        JFrame frame = new JFrame("2023 International Cricket Records Summary");
        frame.setLayout(new BorderLayout());

        JLabel headingLabel = new JLabel("2023 International Cricket Records Summary", SwingConstants.CENTER);
        headingLabel.setFont(new Font("Arial", Font.BOLD, 18));
        headingLabel.setBorder(BorderFactory.createEmptyBorder(40, 0, 0, 0));

        frame.add(headingLabel, BorderLayout.NORTH);

        JPanel mainPanel = new JPanel(new GridBagLayout());
        frame.add(mainPanel, BorderLayout.CENTER);

        // Display Cricket Teams Button
        JButton displayTeamsButton = createStyledButton("Display Cricket Teams");
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        mainPanel.add(displayTeamsButton, gbc);

        displayTeamsButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                Cricket06.displayTeamNames();
            }
        });

        // Display Display Serial /  Parallel Team Button
        JButton displayRecordsParallelButton = createStyledButton("Display Serial /  Parallel Team");
        gbc.gridy = 1; 
        mainPanel.add(displayRecordsParallelButton, gbc);

        displayRecordsParallelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                Cricket07.run(); 
            }
        });
        
    
        
        // Display Team Names containing 'e' Button
        JButton displayTeamNamesContaining = createStyledButton("Display Team Names containing 'e'");
        gbc.gridy = 2; 
        mainPanel.add(displayTeamNamesContaining, gbc);

        displayTeamNamesContaining.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                Cricket08.run(); 
            }
        });

        // Display Team Names with 'e' replaced Button 
        JButton displayTeamNamesReplaced = createStyledButton("Display Team Names with 'e' replaced");
        gbc.gridy = 3; 
        mainPanel.add(displayTeamNamesReplaced, gbc);

        displayTeamNamesReplaced.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                Cricket09.run(); 
            }
        });
        
        // Display Team For Cricket Team Performance Review Button 
        JButton displayTeamWinSummary = createStyledButton("Display Team Performance Review");
        gbc.gridy = 4; 
        mainPanel.add(displayTeamWinSummary, gbc);

        displayTeamWinSummary.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                Cricket01.run(); 
            }
        });
        
        // Display Team Performance Serial /  Parallel Button 
        JButton displayTeamWinSummarySerialParallel = createStyledButton("Display Team Performance Serial /  Parallel");
        gbc.gridy = 5; 
        mainPanel.add(displayTeamWinSummarySerialParallel, gbc);

        displayTeamWinSummarySerialParallel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                Cricket02.run(); 
            }
        });
        
        
        
        // Display Lowest Number Of Points Button 
        JButton displaylowestNumberOfPoint = createStyledButton("Display Lowest Number Of Points");
        gbc.gridy = 6; 
        mainPanel.add(displaylowestNumberOfPoint, gbc);

        displaylowestNumberOfPoint.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                Cricket03.run(); 
            }
        });
        
        // Display Several Teams Points Button 
        JButton displaySeveralTeamsPoints = createStyledButton("Display Several Teams Points");
        gbc.gridy = 7; 
        mainPanel.add(displaySeveralTeamsPoints, gbc);

        displaySeveralTeamsPoints.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                Cricket04.run(); 
            }
        });
        
        // Display Sorted by Comparator in Team Button 
        JButton displaySortedbyComparator = createStyledButton("Display Sorted by Comparator in Team Points");
        gbc.gridy = 8; 
        mainPanel.add(displaySortedbyComparator, gbc);

        displaySortedbyComparator.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                Cricket05.run(); 
            }
        });
        
        
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private static JButton createStyledButton(String text) {
        JButton button = new JButton(text);
        button.setFont(new Font("Arial", Font.BOLD, 14));
        button.setBackground(Color.RED);
        button.setForeground(Color.WHITE);
        button.setFocusPainted(false);
        button.setPreferredSize(new Dimension(200, 80));
        return button;
    }
}
