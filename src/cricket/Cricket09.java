package cricket;

import java.util.Arrays;
import java.util.List;


public class Cricket09 {
	 public static void run() {
		List<Team> table = Arrays.asList(
     	       new Team(1, "India", 22, 18, 2, 2, 800, 400, 400, 50, 20, 8, 2, 90),
                new Team(2, "Australia", 22, 17, 2, 3, 850, 420, 430, 45, 30, 7, 1, 88),
                new Team(3, "England", 22, 15, 3, 4, 700, 450, 250, 35, 40, 5, 3, 78),
                new Team(4, "South Africa", 22, 14, 3, 5, 780, 480, 300, 40, 35, 5, 5, 74),
                new Team(5, "New Zealand", 22, 14, 2, 6, 760, 490, 270, 42, 40, 6, 6, 74),
                new Team(6, "West Indies", 22, 11, 5, 6, 750, 550, 200, 70, 50, 9, 2, 69),
                new Team(7, "Pakistan", 22, 10, 4, 8, 650, 600, 50, 65, 60, 5, 4, 74), 
                new Team(8, "Sri Lanka", 22, 9, 3, 10, 600, 550, 50, 50, 55, 4, 6, 74), 
                new Team(9, "Bangladesh", 22, 8, 2, 12, 550, 580, -30, 45, 65, 3, 5, 48),
                new Team(10, "Afghanistan", 22, 6, 3, 13, 500, 620, -120, 40, 70, 2, 4, 39),
                new Team(11, "Zimbabwe", 22, 4, 2, 16, 450, 680, -230, 35, 80, 2, 6, 26),
                new Team(12, "Ireland", 22, 1, 0, 21, 300, 900, -600, 15, 120, 0, 0, 5));

	
        
		 System.out.println("Team Names with 'e' replaced\n---------");
	        table.stream()
	                .map(team -> team.getTeamName().replaceAll("e", ""))
	                .forEach(modifiedTeamName -> System.out.println(modifiedTeamName));
    }
	
	
	 public static void main(String[] args) {
		 run();
	    }
	
}
