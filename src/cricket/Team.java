package cricket;

public class Team implements Comparable<Team> {
	  private int position;
	    private String teamName;
	    private int matchesPlayed;
	    private int matchesWon;
	    private int matchesDrawn;
	    private int matchesLost;
	    private int runsFor;
	    private int runsAgainst;
	    private int runsDifference;
	    private int wicketsFor;
	    private int wicketsAgainst;
	    private int bonusPoints;
	    private int penaltyPoints;
	    private int totalPoints;

	    public Team(int position, String teamName, int matchesPlayed, int matchesWon,
	    			int matchesDrawn, int matchesLost, int runsFor, int runsAgainst,
	                int runsDifference, int wicketsFor, int wicketsAgainst,
	                int bonusPoints, int penaltyPoints, int totalPoints) {
	    	
	        this.position = position;
	        this.teamName = teamName;
	        this.matchesPlayed = matchesPlayed;
	        this.matchesWon = matchesWon;
	        this.matchesDrawn = matchesDrawn;
	        this.matchesLost = matchesLost;
	        this.runsFor = runsFor;
	        this.runsAgainst = runsAgainst;
	        this.runsDifference = runsDifference;
	        this.wicketsFor = wicketsFor;
	        this.wicketsAgainst = wicketsAgainst;
	        this.bonusPoints = bonusPoints;
	        this.penaltyPoints = penaltyPoints;
	        this.totalPoints = totalPoints;
	        
	    }
	    
	    public String toString() {
	    	 return String.format("%-3d%-20s%10d%10d%10d%10d", position, teamName, matchesPlayed,
		        		matchesWon, matchesDrawn, matchesLost);
	    }
	    
	    
	    public int getPosition() {
	        return position;
	    }

	    public void setPosition(int position) {
	        this.position = position;
	    }

	    public String getTeamName() {
	        return teamName;
	    }

	    public void setTeamName(String teamName) {
	        this.teamName = teamName;
	    }

	    public int getMatchesPlayed() {
	        return matchesPlayed;
	    }

	    public void setMatchesPlayed(int matchesPlayed) {
	        this.matchesPlayed = matchesPlayed;
	    }

	    public int getMatchesWon() {
	        return matchesWon;
	    }

	    public void setMatchesWon(int matchesWon) {
	        this.matchesWon = matchesWon;
	    }

	    public int getMatchesDrawn() {
	        return matchesDrawn;
	    }

	    public void setMatchesDrawn(int matchesDrawn) {
	        this.matchesDrawn = matchesDrawn;
	    }

	    public int getMatchesLost() {
	        return matchesLost;
	    }

	    public void setMatchesLost(int matchesLost) {
	        this.matchesLost = matchesLost;
	    }

	    public int getRunsFor() {
	        return runsFor;
	    }

	    public void setRunsFor(int runsFor) {
	        this.runsFor = runsFor;
	    }

	    public int getRunsAgainst() {
	        return runsAgainst;
	    }

	    public void setRunsAgainst(int runsAgainst) {
	        this.runsAgainst = runsAgainst;
	    }

	    public int getRunsDifference() {
	        return runsDifference;
	    }

	    public void setRunsDifference(int runsDifference) {
	        this.runsDifference = runsDifference;
	    }

	    public int getWicketsFor() {
	        return wicketsFor;
	    }

	    public void setWicketsFor(int wicketsFor) {
	        this.wicketsFor = wicketsFor;
	    }

	    public int getWicketsAgainst() {
	        return wicketsAgainst;
	    }

	    public void setWicketsAgainst(int wicketsAgainst) {
	        this.wicketsAgainst = wicketsAgainst;
	    }

	    public int getBonusPoints() {
	        return bonusPoints;
	    }

	    public void setBonusPoints(int bonusPoints) {
	        this.bonusPoints = bonusPoints;
	    }

	    public int getPenaltyPoints() {
	        return penaltyPoints;
	    }

	    public void setPenaltyPoints(int penaltyPoints) {
	        this.penaltyPoints = penaltyPoints;
	    }

	    public int getTotalPoints() {
	        return totalPoints;
	    }

	    public void setTotalPoints(int totalPoints) {
	        this.totalPoints = totalPoints;
	    }

	    public int compareTo(Team t) {
	        return Integer.compare(totalPoints, t.totalPoints);
	    }    
	
}
